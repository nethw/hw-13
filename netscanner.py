import scapy.all as scapy
import PySimpleGUI as sg
import socket

#Your network ip
NIP = "192.168.121.235"

#Your device details
DEVIP = "192.168.121.125"
DEVMAC = "46:fa:be:9b:dd:01"
DEVNAME = "MacBook Pro — Igor"

window = sg.Window("LNS", [
    [sg.Output(size=(100, 20))], [[sg.ProgressBar(10, size=(54, 20), key="sgBar", bar_color="white", expand_x=True)], [sg.Button("Start", button_color="black", expand_x=True)]]
], background_color="orange")

appStart = True

while True:
    button, _ = window.read(timeout=100)

    if button in (None, "Exit"):
        window.close()
        break

    if appStart:
        print(f'{"":30}{"Your device IP":45}{"Your device MAC":45}{"Your device name":45}\n')
        appStart = False
    
    if button == "Start":
        window["sgBar"].UpdateBar(0, 10)
        clients = [{"iad": scapsr[1].psrc, "mad": scapsr[1].hwsrc} for scapsr in 
            scapy.srp(scapy.Ether(dst="ff:ff:ff:ff:ff:ff") / scapy.ARP(pdst=(f"{NIP}/24")), timeout = 1, verbose = False)[0]]
        window["sgBar"].UpdateBar(0, len(clients))
        print(f"Your device:\n{'':30}{DEVIP:45}{DEVMAC:45}{DEVNAME:45}\nLocal network:")

        if len(clients) == 1:
            print("It seems that your device is alone here(")
        else:
            for i in range(len(clients)):
                if clients[i]["iad"] != DEVIP:
                    try:
                        hn = socket.gethostbyaddr(clients[i]["iad"])[0]
                    except Exception:
                        hn = "unknown"
                    iad = str(clients[i]["iad"])
                    mad = str(clients[i]["mad"])
                    print(f"{'':30}{iad:45}{mad:45}{str(hn):45}")
                    window["sgBar"].UpdateBar(i + 1)
                else:
                    window["sgBar"].UpdateBar(i + 1)
        print()
